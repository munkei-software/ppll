# ppll

`ppll` is a tool to control the execution of commands. It can run commands in
parallel, construct commands from lists of parameters, and more.

It handles the output of commands and can prefix lines with which command
produced it, print timestamps, etc.

`ppll` has functionality similar to `xargs` and `parallel`.

More documentation is available on [MetaCPAN].

## Installation

To install this module, run the following commands:

```sh
perl Build.PL
./Build
./Build test
./Build install
```

Or, if you use [cpanminus] (and, arguably, you should):

```sh
cpanm App::ppll
```

## Support and Documentation

After installing, you can find documentation for this module with the perldoc
command:

```sh
perldoc App::ppll
```

Or:

```sh
ppll --help
```

You can also look for information at:

-   [GitLab]

-   [MetaCPAN]

## License and Copyright

Copyright (C) 2019 Theo Willows

This program is free software; you can redistribute it and/or modify it under
the terms of either: the GNU General Public License as published by the Free
Software Foundation; or the Artistic License.

See <http://dev.perl.org/licenses/> for more information.

[cpanminus]: https://metacpan.org/pod/App::cpanminus
[gitlab]: https://gitlab.com/munkei-software/ppll
[metacpan]: https://metacpan.org/pod/App::ppll
